# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 06:36:42 2020

@author: Administrator
"""
import os
import numpy as np

from matplotlib import pyplot
from PIL import Image
from scipy.spatial.distance import cosine
import os
import sys
import re
import gc
from numpy import asarray
import cv2
import time

import pickle
from keras_vggface.vggface import VGGFace
from keras_vggface.utils import preprocess_input
from keras_vggface.utils import decode_predictions
from keras import backend as K
from mtcnn.mtcnn import MTCNN 

train_folder = "D:/Google Drive/Python_NN/lfw-deepfunneled/"
#train_folder = "C:/Users/Administrator/Google Drive (napa.s@rmutsb.ac.th)/Python_NN/MCYTData/Training/"
val_folder = "D:/Google Drive/Python_NN/lfw-deepfunneled/"
save_path = "D:/Google Drive/Python_NN/lfw_data/"

def loadimgs(path,n = 0):
    '''
    path => Path of train directory or test directory
    '''
    X=[]
    y = []
    cat_dict = {}
    lang_dict = {}
    curr_y = n
    im_count = {}
    im_dir = {}
    # we load every alphabet seperately so we can isolate them later
    for directory_ in os.listdir(path):
        dir_ = {}
        print("loading alphabet: " + directory_)
        lang_dict[directory_] = [curr_y,None]
        directory_path = os.path.join(path,directory_)
        print('---------directory name--------------------')
        print(directory_path)
        im_count[directory_] = 0
        count = 0
        # every letter/category has it's own column in the array, so  load seperately
        for filename in os.listdir(directory_path):
            cat_dict[curr_y] = (directory_, filename)
            category_images=[]
            filename_path = os.path.join(directory_path, filename)
            print('---------file name--------------------')            
            print(filename_path)
            im_count[directory_] = im_count[directory_]+1   
            dir_[count] =   filename_path   
            count = count+1             
        im_dir[directory_] = directory_path;                    
            
 #   y = np.vstack(y)
 #   X = np.stack(X)
    return im_dir, im_count

#------------------------------------------------------------------------
def extract_face(dir_,filename,resolution):
	   
	required_size=(224, 224)
	# load image from file
	print('***********extract_face*****************')
	print(filename)    
	pixels = pyplot.imread(filename)
    
#	print('pixels')    
	# create the detector, using default weights
	detector = MTCNN()
	# detect faces in the image
	print('detector')    
	results = detector.detect_faces(pixels)
#	print(results)
	# extract the bounding box from the first face
	if len(results)>0:
		x1, y1, width, height = results[0]['box']
		if y1<0:
			y1=0
		if x1<0:
			x1=0
		x2, y2 = x1 + width, y1 + height
	# extract the face
		face = pixels[y1:y2, x1:x2]
		save_path = "D:/Google Drive/Python_NN/lfw_face_10"
		cv2.imwrite(save_path+'/test_.jpg', face)
		path_, file_ = os.path.split(os.path.abspath(filename))
		filename_path = os.path.join(save_path, file_)
		os.rename(save_path+'/test_.jpg',filename_path)
#filename.encode('iso8859-11').decode('utf-8')
	# resize pixels to the model size 
#    if x1<0 or y1<0:
#        pyplot.imshow(face)
		resize_ = (resolution, resolution)
		image = Image.fromarray(face)    
		image = image.resize(resize_)
		image = image.resize(required_size)
        
		face_array = asarray(image)
	else:
		print("No face found")
		face_array = np.zeros((224,224,3),dtype=np.uint8)
	K.clear_session() 
	return face_array
    
#------------------------------------------------------------------------
if __name__ == '__main__':

    im_dir, im_count =loadimgs(train_folder)
    count = 0    
    num_min = 10
    for item in im_count:    
        if (im_count[item]>=num_min): 
            print(item)
            count = count +1
            directory_path = im_dir[item]
            for filename in os.listdir(directory_path):
                filename_path = directory_path + '/' + filename
                face_array = extract_face(directory_path,filename_path,224)
    print(count)
 




#    print(im_count[item])
    


%clear
clc
load_ = true;%false;% 
%thr = 0.40;%0.40%0.33%0.40%0.35 %0.33 %0.35 thr for sample selection    
num_train_cand = 10 ;
Num_train = 3;%10;
% if load_ 
% %    load data_senet
% %    load data_facenet
%     load data.mat
%     thr = 0.34%0.40%0.33%0.40%0.35 %0.33 %0.35 thr for sample selection    
%     num_train_cand = 10 
%     Num_train = 3;%10;
% else
OS = 'win';
Mode  = 'norandom';
min_sample_ = 10;

size_ = 256;
noise_ = 0;
Num_known_user = 50%26
target_far = 5% percentage
%kernal_size = 1;
k = 1;
if exist('size_','var') ~=1
    size_ = 32;
end
if exist('noise_','var') ~=1
    noise_ = 20;
end
Training_mode = 'Select'; % 'Select' 'First'

disp(['*************************************']);
disp(['Num_train: ' num2str(Num_train)]);
disp(['k (k-NN: ' num2str(k)]);
disp(['Training_mode: ' Training_mode]);

%if (size_ ==0)
%    filename  = ['data.mat'];%
%elseif noise_ == 0
%    filename  = ['data_' num2str(size_)  '.mat'];%
%else
if (min_sample_ == 10)
%     filename  = ['data_10_' num2str(size_) '_' num2str(noise_) '.mat'];
%     filename  = ['data_openface_10_' num2str(96) '_' num2str(0) '.mat'];
     filename  = ['data__senet50_10_' num2str(224) '_' num2str(0) '.mat'];
%     filename  = ['data_FaceNet_10_' num2str(224) '_' num2str(0) '.mat'];

     
     %    filename  = ['data_10_blur_' num2str(kernal_size) '.mat'];

else    
    filename  = ['data_' num2str(size_) '_' num2str(noise_) '.mat'];
end
%end
disp(['filename 1: ' filename]);
[dir_,escape,embeddings,images] = call_dir(OS,filename);
%[ label_noisy_id] = extract_data(embeddings,images,dir_);
[label,user_id,username] = getLabel(images);

Result_list_facevgg_all = getDistance(embeddings);
%load temp
%end

Result_list_facevgg_original = Result_list_facevgg_all;
label_all = label;
label_noisy_id  = [];%[label_noisy_id  label_noisy_id_2];

%*****************Get user id for known and unknown group******************

%Min_sample = 10;
%id_known = user_id(user_id(:,2)>=Min_sample,1);

Min_sample = 16; %first ten are for training, the rest are for testing
id_known = user_id(user_id(:,2)>=Min_sample,1);

samples_train_cand = []; 
samples_test = [];
testnum = 0;
for i = 1:length(id_known)
    temp_samples = find(label==id_known(i));
    testnum = testnum + length(temp_samples);
    p = randperm(length(temp_samples));
    train_idx = p(1:num_train_cand);
    test_idx = p(num_train_cand+1:end);
    
    samples_train_cand = [samples_train_cand temp_samples(train_idx)'];
    samples_test = [samples_test temp_samples(test_idx)'];
    
end
%*****************get image id for training and test***********************
sample_ID  = setdiff(1:length(label_all),label_noisy_id);
%samples_test = setdiff(sample_ID,samples_train_cand);

%samples_test = samples_test
setdiff(sample_ID,samples_train_cand);
%thr_up = 0.32;
disp('************closed set identification --all users********************') 
%clear samples_train samples_test
%if strcmp(Training_mode,'First')
    [samples_train_first, samples_test_] = get_training_id(samples_train_cand,label_all,id_known,Num_train,Mode);

    [samples_train_first_10, samples_test_] = get_training_id(samples_train_cand,label_all,id_known,10,Mode);
    
    %[samples_train, samples_test] = get_training_id_best(sample_ID,label_all,Result_list_facevgg_original,user_id(:,1));
%[samples_train, samples_test] = get_training_id_first_last(sample_ID,label_all,user_id(:,1));
%elseif strcmp(Training_mode,'Select')
    [samples_train_select,samples_test_] = get_training_id_thr_num(samples_train_cand,label,Result_list_facevgg_original,id_known,thr,Num_train);
%end
clear samples_test_

num_train_user = hist(label(samples_train_select),1:158);
user_interest = find(num_train_user==3);%id_known

%samples_test = setdiff(length(label)+1:length(label_all),label_noisy_id);



disp(['---------------First three samples---------------']) ;
 [rank_userID, score1,label_assigned,score_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first, samples_test,k);
 
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr]  =  class_accuracy(label_all,label_assigned,samples_test, user_interest); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
result.close(1,:) = [mu_fnmr mu_fmr];

disp(['---------------First ten samples---------------']) ;
 [rank_userID, score1,label_assigned,score_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first_10, samples_test,k);
 
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr]  =  class_accuracy(label_all,label_assigned,samples_test, user_interest); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
result.close(3,:) = [mu_fnmr mu_fmr];

disp(['---------------Select three sample' num2str(num_train_user(id_known))]) ;
[rank_userID, score1,label_assigned,score_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_select, samples_test,k);
 
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr]  =  class_accuracy(label_all,label_assigned,samples_test, user_interest ); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
result.close(2,:) = [mu_fnmr mu_fmr];
result.close_header = ["mu_fnmr" 'mu_fmr'];


% [rank_userID, score3,label_assigned,score_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train, samples_test,3);
% [mu_fnmr, sd_fnmr, mu_fmr, sd_fmr] =  class_accuracy(label_all,label_assigned,samples_test ); 
% disp(['3-NN FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
% 
% [rank_userID, score5,label_assigned,score_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train, samples_test,10);
% [mu_fnmr, sd_fnmr, mu_fmr, sd_fmr] =  class_accuracy(label_all,label_assigned,samples_test ); 
% disp(['10-NN FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;


 disp('************open set identification --only known ID users********************') 
sample_ID  = setdiff(1:length(label_all),label_noisy_id);

 %temp = round(size(id_known)*.66);
%id_known=user_id(1:Num_known_user,1);
id_unknown = setdiff(user_id(:,1),id_known);

% clear samples_train samples_test
% if strcmp(Training_mode,'First')
%     [samples_train, samples_test] = get_training_id(sample_ID,label_all,id_known,Num_train,Mode);
% %[samples_train, samples_test] = get_training_id_best(sample_ID,label_all,Result_list_facevgg_original,id_known);
% %[samples_train, samples_test] = get_training_id_first_last(sample_ID,label_all,id_known);
% elseif strcmp(Training_mode,'Select')
%     [samples_train,samples_test] = get_training_id_thr_num(sample_ID,label,Result_list_facevgg_original,id_known,thr,Num_train);
% end

samples_test_known = samples_test;%samples_test(ismember(label_all(samples_test),id_known));
%samples_test_unknown = setdiff(samples_test,samples_test_known);
samples_test = setdiff(sample_ID,samples_train_cand);

samples_test_unknown = find(ismember(label_all,id_unknown));%setdiff(samples_test,samples_test_known);

disp(['---------------First three samples---------------']) ;
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first, samples_test,k);
positive = score(samples_test_known,1); 
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first, samples_test_unknown,k);
negative = score(samples_test_unknown,1);

[TPR FPR th EER thr_] = ROC(positive,negative,1);
disp(['threshold veri:' num2str(thr_) ', EER:' num2str(EER)]);
Result_list_facevgg_temp = Result_list_facevgg_all;
Result_list_facevgg_temp(Result_list_facevgg_temp>thr_)= inf;

[rank_userID, score1,label_assigned1,score_assigned1] = classify_samples(Result_list_facevgg_temp, label_all, samples_train_first, samples_test,k);
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr, mu_frr, sd_frr, TRR, FNMR, FMR] =  class_accuracy_open(label_all,label_assigned1,samples_test,samples_test_unknown, user_interest); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
disp(['TRR = ',num2str(TRR) ]) ;
result.open(1,:) = [mu_fnmr mu_fmr TRR];

disp(['---------------First ten samples---------------']) ;
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first_10, samples_test,k);
positive = score(samples_test_known,1); 
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_first_10, samples_test_unknown,k);
negative = score(samples_test_unknown,1);

[TPR FPR th EER thr_] = ROC(positive,negative,1);
disp(['threshold veri:' num2str(thr_) ', EER:' num2str(EER)]);
Result_list_facevgg_temp = Result_list_facevgg_all;
Result_list_facevgg_temp(Result_list_facevgg_temp>thr_)= inf;

[rank_userID, score1,label_assigned1,score_assigned1] = classify_samples(Result_list_facevgg_temp, label_all, samples_train_first_10, samples_test,k);
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr, mu_frr, sd_frr, TRR, FNMR, FMR] =  class_accuracy_open(label_all,label_assigned1,samples_test,samples_test_unknown, user_interest); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
disp(['TRR = ',num2str(TRR) ]) ;
result.open(3,:) = [mu_fnmr mu_fmr TRR];

disp(['---------------Select three samples---------------']) ;
%  [rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_select, samples_test,k);
% positive = score(samples_test_known,1);
% negative = score(samples_test_unknown,1);
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_select, samples_test,k);
positive = score(samples_test_known,1);
[rank_userID, score,label_assigned] = classify_samples(Result_list_facevgg_all, label_all, samples_train_select, samples_test_unknown,k);
negative = score(samples_test_unknown,1);


[TPR FPR th EER thr_] = ROC(positive,negative,1);
disp(['threshold veri:' num2str(thr_) ', EER:' num2str(EER)]);
Result_list_facevgg_temp = Result_list_facevgg_all;
Result_list_facevgg_temp(Result_list_facevgg_temp>thr_)= inf;

[rank_userID, score1,label_assigned1,score_assigned1] = classify_samples(Result_list_facevgg_temp, label_all, samples_train_select, samples_test,k);
[mu_fnmr, sd_fnmr, mu_fmr, sd_fmr, mu_frr, sd_frr, TRR, FNMR, FMR] =  class_accuracy_open(label_all,label_assigned1,samples_test,samples_test_unknown, user_interest); 
disp(['1st rank FNMR = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
disp(['TRR = ',num2str(TRR) ]) ;

result.open_header = ["mu_fnmr" 'mu_fmr' 'TRR'];
result.open(2,:) = [mu_fnmr mu_fmr TRR];

close
% target_far = target_far/100;
% idx = find(abs(FPR-target_far) == min(abs(FPR-target_far)));%idx = find(abs(FPR-0.05) == min(abs(FPR-0.05)));
% thr_= th(idx);
% thr_oneperc = th(idx(end));
% disp([]);
% disp(['@' num2str(target_far) '% FPR threshold veri:' num2str(thr_oneperc)]); 
% Result_list_facevgg_temp = Result_list_facevgg_all;
% Result_list_facevgg_temp(Result_list_facevgg_temp>thr_oneperc)= inf;
% [rank_userID, score1,label_assigned1,score_assigned1] = classify_samples(Result_list_facevgg_temp, label_all, samples_train, samples_test,k);
% [mu_fnmr, sd_fnmr, mu_fmr, sd_fmr, mu_frr, sd_frr, TRR, FNMR, FMR] =  class_accuracy_open(label_all,label_assigned1,samples_test,samples_test_unknown); 
% disp(['1st rank FNMR @' num2str(target_far) '% = ',num2str(mu_fnmr) ,' ( SD = ', num2str(sd_fnmr),') FMR = ',num2str(mu_fmr) ,' ( SD = ', num2str(sd_fmr),')']) ;
% disp(['TRR @1%= ',num2str(TRR) ]) ;
function [samples_train,samples_test] = get_training_id_thr_num(sample_ID,label,score,id_known,thr,num)
    samples_train=[];
    for i = 1: length(id_known)
        clear temp_samples;
        temp_samples = find(label==id_known(i));
        [C,ia,ib]  = intersect(temp_samples,sample_ID,'stable');  
        temp_samples = sample_ID(sort(ib));
        temp_score = score(temp_samples,temp_samples);
        
                %first sample
        id_(1) = 1;
        sample_id = temp_samples(id_(1));
        samples_train =[samples_train sample_id];
        count = 1;
        %%
        for k = 2:num        
            for j = id_(k-1)+1:length(temp_samples)
                is_valid = true;
                for temp = 1:k-1
                    is_valid = is_valid&& (temp_score(id_(temp),j)>thr);
                    
                end
                if is_valid%(temp_score(id_1,j)>thr) %&& (temp_score(id_1,j)<thr_up)
                    count =count+1;
                    id_(k) = j;
                    sample_id =  temp_samples(id_(k));
                    samples_train =[samples_train sample_id];
                    break;
                end    
            end        
%            if count <num
%            end
             if j == length(temp_samples)
                 break;
             end
            
        end
        %%

        

    end

    samples_test  = setdiff(sample_ID,samples_train);
%    samples_test = setdiff(samples_test,label_noisy_id);%filter noisy label
end

function [samples_train samples_test] = get_training_id_best(sample_ID,label,score,id_known)
    Num_train = 1;
    samples_train=[];
    for i = 1: length(id_known)
        clear temp_samples;
        temp_samples = find(label==id_known(i));
        temp_samples = intersect(temp_samples,sample_ID);        
        temp_score = score(temp_samples,temp_samples);
        mean_score = mean(temp_score);
        idx = find(mean_score==min(mean_score));
        best_sample_id = temp_samples(idx(1));
        
        samples_train =[samples_train best_sample_id];
    end

    samples_test  = setdiff(sample_ID,samples_train);
%    samples_test = setdiff(samples_test,label_noisy_id);%filter noisy label
end
function [samples_train samples_test] = get_training_id_first_last(sample_ID,label,id_known)
    Num_train = 1; 
    samples_train=[];
    for i = 1: length(id_known)
        clear temp_samples;
        temp_samples = find(label==id_known(i));
        temp_samples = intersect(temp_samples,sample_ID);        
%        temp_samples = setdiff(temp_samples,label_noisy_id);%filter noisy label
    
        samples_train =[samples_train temp_samples(1) temp_samples(end)];%:min(Num_train,length(temp_samples)))'];
    end

    samples_test  = setdiff(sample_ID,samples_train);
%    samples_test = setdiff(samples_test,label_noisy_id);%filter noisy label
end

function [samples_train samples_test] = get_training_id(sample_ID,label,id_known,Num_train,Mode)

    samples_train=[];
    for i = 1: length(id_known)
        clear temp_samples;
        temp_samples = find(label==id_known(i));
        [C,ia,ib]  = intersect(temp_samples,sample_ID,'stable');  
        temp_samples = sample_ID(sort(ib))';
%        temp_samples = intersect(temp_samples,sample_ID);        
        if strcmp(Mode,'random')
            if i == 1
                disp('*****random mode****')
            end
            temp_samples = temp_samples(randperm(numel(temp_samples)));%shuffle
        else
            %No shuffle        
        end
%        temp_samples = setdiff(temp_samples,label_noisy_id);%filter noisy label
    
        samples_train =[samples_train temp_samples(1:min(Num_train,length(temp_samples)))'];
    end

    samples_test  = setdiff(sample_ID,samples_train);
%    samples_test = setdiff(samples_test,label_noisy_id);%filter noisy label
end
%*****************Get output**************************
function [label_sort, score_sort,label_assigned,score_weigth] = classify_samples(distance, label, samples_train, samples_test,k)

%Result = Result_list_facevgg(samples_test,samples_train);
    count_known = 1;
    count_unknown = 1;
    Num_match = 5;
%    label_assigned(1,length(distance)) = 0;
%    score_sort(1,length(samples_train)) = 0;
    
    for i = 1: length(samples_test)

        sample_id = samples_test(i);
        result_vec = distance(sample_id,samples_train);
        [a b] = sort(result_vec);
        score_sort(sample_id,:) = a;
        label_sort(sample_id,:) = label(samples_train(b));
        [label_temp score_temp] = majority_weight(label_sort(sample_id,1:k),score_sort(sample_id,1:k));
        if length(label_temp) ~= 1 
            label_assigned
        end
          
        label_assigned(sample_id) = label_temp;
        score_weigth(sample_id) = score_temp;
    end    
end
function [label_assigned score_assigned]= majority_weight(label_sort,score_sort)
    if (sum(score_sort>1)>0)
        sum(score_sort>1);
    end
    label_sort(score_sort>1) =[];
    score_sort(score_sort>1) =[];
    if length(score_sort)>1 && (score_sort(1)>0)       
        sample_weigth =  1./score_sort;
        sample_weigth = sample_weigth./sum(sample_weigth,2);
        score_weigth = (1-score_sort).*sample_weigth;
        for i = 1:33
            weight(i) = sum(score_weigth(label_sort==i));
        end
        label_assigned = find(weight==max(weight));    
        score_assigned = 1-max(weight);    
    else
        if length(score_sort)==0
            label_assigned = 0;       
            score_assigned = 0;            
        else %length(score_sort) == 1 || score_sort(1)==0
            label_assigned = label_sort(1);       
            score_assigned = score_sort(1);
            if length(score_sort)>1
                if score_sort(2)==0
                    score_sort(2)
                end
            end        
        end
    end
end
function Result_list_facevgg = getDistance(embeddings)
%     for i = 1:size(embeddings,1)
%         v1 = embeddings(i,:);        
%         parfor j = 1:size(embeddings,1)            
%             v2 = embeddings(j,:);
%             if i == j 
%                 dist = 0;
%             else               
%                 dist = 1-dot(v1,v2)/norm(v1)/norm(v2);
%             end
%             if (dist<0)
%                 dist = 0;
%             end
%             
%             Result_list_facevgg(i,j) = dist;
%         end
%     end
    Result_list_facevgg  = squareform(pdist(embeddings,'cosine'));
end
function [label_noisy_id ] = extract_data(embeddings,images,dir_miss_face)
    
%    Result_list_facevgg = getDistance(embeddings);
    N_sample = size(images,1);

%****************Get noisy image id****************
    label_noisy = dir(dir_miss_face);
    count = 1;
    for i = 1:length(label_noisy)
        str_curr = strtok(label_noisy(i).name,'.');
        if length(str_curr)>0
            label_noisy_(count)=cellstr(str_curr);
            count = count+1;
            for j = 1:N_sample
                if strcmp(str_curr,strtok(images(j,:),'.'))
                    label_noisy_id(count-1) = j;
                    break;
                end
            end
        end
    end
    clear label_noisy count embeddings i j
%****************remove noisy image distance**********
%label_noisy_id = [];
%    Result_list_facevgg(label_noisy_id,:)=inf;
%    Result_list_facevgg(:,label_noisy_id)=inf;
end
function [label user_id username] = getLabel(images)
%*****************Get subject label of each image**************************
    id = 1;
    [str_int remain_] = strtok(images(1,:),'_');
    last_= strtok(remain_,'_');
    str_int =str_int+"_"+last_;
    
    username(1).name = str_int;
    new_label(1) = 1;
    for i = 2:size(images,1)
        [str_curr remain_] = strtok(images(i,:),'_');
        last_= strtok(remain_,'_');
        str_curr =str_curr+"_"+last_;        
        if ~strcmp(str_int,str_curr)
            id = id+1; 
            str_int = str_curr;
            username(id).name = str_int;        
        end
        new_label(i) = id ;
    end
    label = new_label';
    clear new_label id;
    user_id = [unique(label) histc(label, unique(label))];
end

function [label user_id] = map_Label(images,username)
%*****************Get subject label of each image**************************
    for i = 1:size(images,1)
        [str_int remain_]= strtok(images(i,:),'_');
        last_= strtok(remain_,'_');
        str_int =str_int+"_"+last_;
        label(i,1) = inf;                
        for id =  1:length(username)
            str_curr = username(id).name;            
            if strcmp(str_int,str_curr)
                label(i,1) = id;                                
                break;
            end
        end
    end
    user_id = [unique(label) histc(label, unique(label))];
end
function [dir_ escape embeddings images] = call_dir(OS, filename)
    if strcmp(OS,'mac')
    %    dir_ = '/Volumes/data/Google Drive/face_recog_project/temp1';
        dir_ = ['/Volumes/data/Google Drive/Python_NN' ];
        escape = '/';
    else
        dir_ = ['D:\Google Drive\Python_NN' ]   ;
        escape = '\';
    end
    load([dir_ escape filename]);
    dir_= [dir_ escape 'wrong'];
    
end

function [mu_fnmr, sd_fnmr, mu_fmr, sd_fmr] =  class_accuracy(label_all,label_assigned,samples_test,user_interest )
    all_user = user_interest;%unique(label_all(samples_test,1));
    NUM = length(samples_test);
    for i = all_user
        num_(i) = sum(label_all(samples_test,1)==i);
        
        true_match(i) = sum((label_assigned(samples_test)==i)&(label_all(samples_test)'==i));
        false_match(i) = sum(label_assigned(samples_test)==i)-true_match(i);
        
        FNMR(i) = 1-true_match(i)/num_(i);
        FMR(i) = false_match(i)/(NUM-num_(i)) ;
        
    end
    ID_rate = 100*sum(true_match)/sum(num_);
    mu_fnmr = 100*mean(FNMR(all_user));
    sd_fnmr = 100*std(FNMR(all_user));
    mu_fmr = 100*mean(FMR(all_user));
    sd_fmr = 100*std(FMR(all_user));
%    disp(['IR = ', num2str(ID_rate)]);    
    disp(['class accuracy = ', num2str(100-mu_fnmr) , '(SD = ', num2str(sd_fnmr) ,')']);
    
end

function [mu_fnmr, sd_fnmr, mu_fmr, sd_fmr, mu_frr, sd_frr, TRR, FNMR, FMR] =  class_accuracy_open(label_all,label_assigned,samples_test,samples_test_unknown, user_interest)
    samples_test_known = setdiff(samples_test,samples_test_unknown);
    all_user = user_interest;%unique(label_all(samples_test_known,1));
    NUM = length(samples_test);
    NUM1 = length(samples_test_unknown);
    
%    true_reject_rate
    TRR = 100*sum(label_assigned(samples_test_unknown)==0)/NUM1;
    FNMR = nan*zeros(1,33);
    FMR = nan*zeros(1,33);
    FRR = nan*zeros(1,33);
    
    for i = all_user
        num_(i) = sum(label_all(samples_test,1)==i);
        
        true_match(i) = sum((label_assigned(samples_test)==i)&(label_all(samples_test)'==i));
        false_match(i) = (sum(label_assigned(samples_test)==i)-true_match(i))+sum(label_assigned(samples_test_unknown)==i);
        
        false_reject(i) = sum((label_assigned(samples_test)==0)&(label_all(samples_test)'==i));
        
        
%        false_match(i) = sum(label_assigned(samples_test)==i)-true_match(i);
        
        FNMR(i) = 1-true_match(i)/num_(i);
        FMR(i) = false_match(i)/(NUM+NUM1-num_(i)) ;
        FRR(i) = false_reject(i)/num_(i) ;
    end
    mu_fnmr = 100*mean(FNMR(all_user));
    sd_fnmr = 100*std(FNMR(all_user));
    mu_fmr = 100*mean(FMR(all_user));
    sd_fmr = 100*std(FMR(all_user));
    mu_frr = 100*mean(FRR(all_user));
    sd_frr = 100*std(FRR(all_user));    
    disp(['class accuracy = ', num2str(100-mu_fnmr) , '(% SD = ', num2str(sd_fnmr) ,')']);
    
end

function area = area_triangle(a,b,c)
    s = (a+b+c)/2;
    area = sqrt(s*(s-a)*(s-b)*(s-c));
end

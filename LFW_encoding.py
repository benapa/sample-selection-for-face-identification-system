# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 11:19:30 2020

@author: Napa
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 06:36:42 2020

@author: Administrator
"""
import os
import numpy as np

from matplotlib import pyplot
from PIL import Image
from scipy.spatial.distance import cosine
import os
import sys
import re
import gc
from numpy import asarray
import cv2
import time

import pickle
from keras_vggface.vggface import VGGFace
from keras_vggface.utils import preprocess_input
from keras_vggface.utils import decode_predictions
from keras import backend as K
from mtcnn.mtcnn import MTCNN 
from tensorflow import keras
import noisy
from PIL import Image, ImageFilter
import tensorflow as tf
from keras_facenet import FaceNet

import scipy.io
#------------------------------------------------------------------------
save_path = "D:/Google Drive/Python_NN/lfw_face_10/"
sigma  = 20
required_size=(224, 224)

def get_filename(filenames):
	# extract faces
    
#	print(filenames)
#	print('****************************')

    included_extensions = ['jpg','jpeg', 'bmp', 'png', 'gif']
#	faces = [extract_face(f) for f in filenames if any(f.endswith(ext) for ext in included_extensions)]
    ## option 2
    faces = []
    filename_ = [f for f in filenames if (any(f.endswith(ext) for ext in included_extensions))]
#			filenames.remove(f)
#	for f in filename_:
#		faces.append(extract_face(f))
    return filename_
def get_embeddings_openface(save_path,sigma,resolution):
#    model = model_from_json(open("openface_structure.json", "r").read(), custom_objects={'tf': tf})
    model = keras.models.load_model('model_openface')
    model.load_weights("openface_weights.h5")
    required_size=(96, 96)
#    window = tk.Tk()
    faces = []#= np.empty((224,224,3))
    for filename in os.listdir(save_path):
        filename_path = save_path + '/' + filename
        face = pyplot.imread(filename_path)
        noisy_face = noisy.noisy("gauss",face,sigma)
        noisy_face = noisy_face.astype(np.uint8)
        image = Image.fromarray(noisy_face)    
        image = image.resize(resolution)
        image = image.resize(required_size)
        noisy_face = asarray(image)        
        faces.append(noisy_face)
        print(filename)        
    print('***********get_embeddings*****************')
    
#	with multiprocessing.Pool(5) as q:
#		faces  = q.map(extract_face, files)
# convert into an array of samples

    if True:#len(faces)>0:
        samples = asarray(faces, 'float32')
		# prepare the face for the model, e.g. center pixels
#        samples = preprocess_input(samples, version=2)
		# create a vggface model
#        model = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
	# perform prediction
        embeddings = model.predict(samples)
    else:
        embeddings = 0	
#	K.clear_session()
    file_= os.listdir(save_path)    
    filenames = get_filename(file_)
    scipy.io.savemat('data_openface_10_'+str(resolution[1] )+'_'+str(sigma)+'.mat', mdict={'embeddings': embeddings,'images':filenames})

#    with open('encode_'+str(resolution[1])+'_'+str(sigma) , 'wb') as f:  # Python 3: open(..., 'wb')
#        pickle.dump([filenames, embeddings], f)
        
    return faces, embeddings
def get_embeddings_FaceNet(save_path,sigma,resolution):
#https://machinelearningmastery.com/how-to-develop-a-face-recognition-system-using-facenet-in-keras-and-an-svm-classifier/
#    model = model_from_json(open("openface_structure.json", "r").read(), custom_objects={'tf': tf})
#    model = keras.models.load_model('model_facenet')
#    model.load_weights("facenet_keras_weights.h5")
    embedder = FaceNet()
    required_size=(160, 160)
#    window = tk.Tk()
    faces = []#= np.empty((224,224,3))
    for filename in os.listdir(save_path):
        filename_path = save_path + '/' + filename
        face = pyplot.imread(filename_path)
        noisy_face = noisy.noisy("gauss",face,sigma)
        noisy_face = noisy_face.astype(np.uint8)
        image = Image.fromarray(noisy_face)    
#        image = image.resize(resolution)
        image = image.resize(required_size)
        noisy_face = asarray(image)        
        faces.append(noisy_face)
        print(filename)        
    print('***********get_embeddings*****************')
    
#	with multiprocessing.Pool(5) as q:
#		faces  = q.map(extract_face, files)
# convert into an array of samples

    if True:#len(faces)>0:
        samples = asarray(faces, 'float32')
		# prepare the face for the model, e.g. center pixels
#        samples = preprocess_input(samples, version=2)
		# create a vggface model
#        model = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
	# perform prediction
#        embeddings = model.predict(samples)
        embeddings = embedder.embeddings(samples)

    else:
        embeddings = 0	
#	K.clear_session()
    file_= os.listdir(save_path)    
    filenames = get_filename(file_)
    scipy.io.savemat('data_FaceNet_10_'+str(resolution[1] )+'_'+str(sigma)+'.mat', mdict={'embeddings': embeddings,'images':filenames})

#    with open('encode_'+str(resolution[1])+'_'+str(sigma) , 'wb') as f:  # Python 3: open(..., 'wb')
#        pickle.dump([filenames, embeddings], f)
        
    return faces, embeddings
def get_embeddings_senet50(save_path,sigma,resolution):
    required_size=(224, 224)
#    window = tk.Tk()
    faces = []#= np.empty((224,224,3))
    for filename in os.listdir(save_path):
        filename_path = save_path + '/' + filename
        face = pyplot.imread(filename_path)
        noisy_face = noisy.noisy("gauss",face,sigma)
        noisy_face = noisy_face.astype(np.uint8)
        image = Image.fromarray(noisy_face)    
        image = image.resize(resolution)
        image = image.resize(required_size)
        noisy_face = asarray(image)        
        faces.append(noisy_face)
        print(filename)        
    print('***********get_embeddings*****************')
    
#	with multiprocessing.Pool(5) as q:
#		faces  = q.map(extract_face, files)
# convert into an array of samples

    if True:#len(faces)>0:
        samples = asarray(faces, 'float32')
		# prepare the face for the model, e.g. center pixels
        samples = preprocess_input(samples, version=2)
		# create a vggface model
        model = VGGFace(model='senet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
	# perform prediction
        embeddings = model.predict(samples)
    else:
        embeddings = 0	
#	K.clear_session()
    file_= os.listdir(save_path)    
    filenames = get_filename(file_)
    scipy.io.savemat('data__senet50_10_'+str(resolution[1] )+'_'+str(sigma)+'.mat', mdict={'embeddings': embeddings,'images':filenames})

#    with open('encode_'+str(resolution[1])+'_'+str(sigma) , 'wb') as f:  # Python 3: open(..., 'wb')
#        pickle.dump([filenames, embeddings], f)
        
    return faces, embeddings

def get_embeddings(save_path,sigma,resolution):
    required_size=(224, 224)
#    window = tk.Tk()
    faces = []#= np.empty((224,224,3))
    for filename in os.listdir(save_path):
        filename_path = save_path + '/' + filename
        face = pyplot.imread(filename_path)
        noisy_face = noisy.noisy("gauss",face,sigma)
        noisy_face = noisy_face.astype(np.uint8)
        image = Image.fromarray(noisy_face)    
        image = image.resize(resolution)
        image = image.resize(required_size)
        noisy_face = asarray(image)        
        faces.append(noisy_face)
        print(filename)        
    print('***********get_embeddings*****************')
    
#	with multiprocessing.Pool(5) as q:
#		faces  = q.map(extract_face, files)
# convert into an array of samples

    if True:#len(faces)>0:
        samples = asarray(faces, 'float32')
		# prepare the face for the model, e.g. center pixels
        samples = preprocess_input(samples, version=2)
		# create a vggface model
        model = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
	# perform prediction
        embeddings = model.predict(samples)
    else:
        embeddings = 0	
#	K.clear_session()
    file_= os.listdir(save_path)    
    filenames = get_filename(file_)
    scipy.io.savemat('data_10_'+str(resolution[1] )+'_'+str(sigma)+'.mat', mdict={'embeddings': embeddings,'images':filenames})

#    with open('encode_'+str(resolution[1])+'_'+str(sigma) , 'wb') as f:  # Python 3: open(..., 'wb')
#        pickle.dump([filenames, embeddings], f)
        
    return faces, embeddings
def get_embeddings_blur(save_path,kernal):
    required_size=(224, 224)
#    window = tk.Tk()
    faces = []#= np.empty((224,224,3))
    for filename in os.listdir(save_path):
        filename_path = save_path + '/' + filename
        face = Image.open(filename_path)
        blur_face = face.filter(ImageFilter.GaussianBlur(kernal))
        blur_face_np = np.array(blur_face)
        blur_face_np = blur_face_np.astype(np.uint8)
        image = Image.fromarray(blur_face_np)    
        image = image.resize(required_size)
        blur_face_np = asarray(image)        
        faces.append(blur_face_np)
        print(filename)        
    print('***********get_embeddings*****************')
    
#	with multiprocessing.Pool(5) as q:
#		faces  = q.map(extract_face, files)
# convert into an array of samples

    if True:#len(faces)>0:
        samples = asarray(faces, 'float32')
		# prepare the face for the model, e.g. center pixels
        samples = preprocess_input(samples, version=2)
		# create a vggface model
        model = VGGFace(model='resnet50', include_top=False, input_shape=(224, 224, 3), pooling='avg')
	# perform prediction
        embeddings = model.predict(samples)
    else:
        embeddings = 0	
#	K.clear_session()
    file_= os.listdir(save_path)    
    filenames = get_filename(file_)
    scipy.io.savemat('data_10_blur_'+str(kernal)+'.mat', mdict={'embeddings': embeddings,'images':filenames})

#    with open('encode_'+str(resolution[1])+'_'+str(sigma) , 'wb') as f:  # Python 3: open(..., 'wb')
#        pickle.dump([filenames, embeddings], f)
        
    return faces, embeddings
if __name__ == '__main__':
#        faces = np.stack((faces,noisy_face),3)        
    sigma  = 0
    resolution=(224, 224)    
    resolution_openface=(96, 96)    

    [faces, embeddings] = get_embeddings_FaceNet(save_path,sigma,resolution)
#    for resolution in {32,64,128,256}:
#        for sigma in {0,5,10,20}:
#            [faces, embeddings] = get_embeddings(save_path,sigma,(resolution,resolution))
#    for kernal in {3,5,7}:
#            [faces, embeddings] = get_embeddings_blur(save_path,kernal)




#    print(im_count[item])
    
